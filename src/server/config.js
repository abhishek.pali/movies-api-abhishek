module.exports = {
    mysql: {
        host: process.env.MYSQL_HOST,
        database: process.env.MYSQL_DATABASE,
        username: process.env.MYSQL_USERNAME,
        password: process.env.MYSQL_PASSWORD,
        timeout: 1000, // milli-seconds
        pool: {
            max: Number(process.env.MYSQL_POOL),
            min: 0,
            acquire: 30000, // milli-seconds
            idle: 10000, // milli-seconds
        },
    },
    port: process.env.PORT || 5000,
};
