const { Pool } = require("pg");
const credentials = require('./credentials');

const pool = new Pool({
    user: credentials.username,
    password: credentials.password,
    database: credentials.database,
    host: credentials.server
});

pool.connect(function(err){
    if(err)
    {
        console.log(`Error occurred while connecting \n${err}`);
    }
  });

module.exports = {
    getDirectors: function() {
        return new Promise(function(resolve, reject) {
          const directorsQuery = 'SELECT director_name FROM directors';
          pool.query(directorsQuery).then(function(res) {
            resolve(res.rows);
          }).catch(function(err) {
            reject(err);
          });
        });
      },
    
      getDirectorWithID: function(app) {
          return new Promise(function(resolve, reject) {
              app.get('/api/directors/:directorId', function(req, res) {
                const directorIdQuery = `SELECT director_name FROM directors WHERE id = ${req.params.directorId}`;
                pool.query(directorIdQuery).then(function(res1) {
                    res.send(res1.rows);
                    resolve(res1.rows);
                }).catch(function(err) {
                    reject(err);
                });
            });
        });  
    },

    addDirector: function(app) {
      return new Promise(function(resolve, reject) {
        app.post('/api/directors', function(req, res) {
            const data = {
              name: req.body.directorName
            }
            const addDirectorQuery = 'INSERT INTO directors(director_name) VALUES($1)';
            const value = [data.name];
            pool.query(addDirectorQuery, value).then(function(res1) {
              res.send(res1)
              resolve(res1);
            }).catch(function(err) {
              reject(err);
            });
        });
      });
    },

    updateDirectorWithId: function(app) {
      return new Promise(function(resolve, reject) {
        app.put('/api/directors/:directorId', function(req, res) {
          const directorIdQuery = `UPDATE directors set director_name = '${req.body.directorName}' WHERE id = ${req.params.directorId}`;
          pool.query(directorIdQuery).then(function(res1) {
              res.send(res1);
              resolve(res1);
          }).catch(function(err) {
              reject(err);
          });
      });
  });  
    },

    deleteDirectorWithId: function(app) {
      return new Promise(function(resolve, reject) {
        app.delete('/api/directors/:directorId', function(req, res) {
          const directorIdQuery = `DELETE FROM directors where id = ${req.params.directorId}`;
          pool.query(directorIdQuery).then(function(res1) {
              res.send(res1);
              resolve(res1);
          }).catch(function(err) {
              reject(err);
          });
      });
  });  
}

}