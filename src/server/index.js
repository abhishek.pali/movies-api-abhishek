const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
const directors = require("./directors");
const movies = require("./movies");
var path = require("path");

directors.getDirectors().then(function(res1) {
  app.get('/api/directors', function (req, res) {
    res.send(res1);   
  });
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

directors.getDirectorWithID(app).then(function(data) {
  console.log(`Here is the director that you were looking for :`);
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

directors.addDirector(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

directors.updateDirectorWithId(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

directors.deleteDirectorWithId(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

movies.getMovies().then(function(res1) {
  app.get('/api/movies', function (req, res) {
      res.send(res1);
  });
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

movies.getMovieWithID(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

movies.addMovie(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

movies.updateMovieWithId(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

movies.deleteMovieWithId(app).then(function(data) {
  console.log(data);
}).catch(function(err) {
  console.log(`Oops. There is something wrong with the query.\n ${err}`);
});

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../public','/index.html'), function (err) {
    if (err) {
      next(err)
    } else {
      console.log('Root page working fine');
    }
  });
});


// app.use((req, res, next) => {
//   const error = new Error("Page not found");
//   error.status = 404;
//   next(error);
// });

// // error handler middleware
// app.use((error, req, res, next) => {
//     res.status(error.status || 500).send({
//       error: {
//         status: error.status || 500,
//         message: error.message || 'Internal Server Error'
//       }
//     });
//   });

app.listen(5000, () => console.log(`listening to port 5000!`));