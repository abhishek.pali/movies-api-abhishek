const Sequelize = require('sequelize');

const Model = Sequelize.Model;

module.exports = function(sequelize) {
    class Directors extends Model { }
    Directors.init({
        // attributes
        director_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        director_name: {
            type: Sequelize.STRING
            // allowNull defaults to true
        }
    }, {
        sequelize,
        modelName: 'directors'
    });

    return Directors;
}
