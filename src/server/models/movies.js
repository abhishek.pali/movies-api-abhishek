const Sequelize = require('sequelize');

const Model = Sequelize.Model;

module.exports = function(sequelize) {
    class Movies extends Model { }
    Movies.init({
        // attributes
        rank: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        title: {
            type: Sequelize.TEXT
        },
        description: {
            type: Sequelize.TEXT
        },
        runtime: {
            type: Sequelize.INTEGER
        },
        genre: {
            type: Sequelize.TEXT
        },
        rating: {
            type: Sequelize.FLOAT
        },
        metascore: {
            type: Sequelize.INTEGER
        },
        votes: {
            type: Sequelize.BIGINT
        },
        gross_earning_in_mil: {
            type: Sequelize.FLOAT
        },
        actor: {
            type: Sequelize.TEXT
        },
        year: {
            type: Sequelize.INTEGER
        },
        director_id: {
            type: Sequelize.INTEGER
        }
    }, {
        sequelize,
        modelName: 'movies'
    });

    return Movies;
}
