// creating a pool to interact with postgres database
const { Pool } = require("pg");
const credentials = require("./credentials");
var csv = require("fast-csv");
var config = {
    user: credentials.username,
    password: credentials.password,
    database: credentials.database,
    host: credentials.server,
    port: 5432,
    max: 100
};
const pool = new Pool(config);

var directorMap = {};

pool.connect(function(err){
  if(err)
  {
      console.log(`Error occurred while connecting \n${err}`);
  }
});

// moviesCsvStream to stream/read through the data inside movies csv
// {headers: true } means treat the 1st line of csv as headers
let moviesCsvStream = csv.parseFile("../data/movies.csv", { headers: true }).on("data", function(record) {
    // pause after reading the first line of data
    // each line of data is called as record
    moviesCsvStream.pause();
    // storing the data in each line in a variable
    let rank = record.Rank;
    let title = record.Title;
    let description = record.Description;
    let runtime = record.Runtime;
    let genre = record.Genre;
    let rating = record.Rating;
    let metascore = record.Metascore;
    let votes = record.Votes;
    let gross_earning_in_mil = record.Gross_Earning_in_Mil;
    let director = record.Director;
    let actor = record.Actor;
    let year = record.Year;
    // calling a query to store the data in their respective column ids inside the
    // movies table with error handling 
    pool.query("INSERT INTO movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,Director,Actor,Year) \ VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)", 
    [rank,title,description,runtime,genre,rating,metascore,votes,gross_earning_in_mil,director,actor,year], function(err){
        if(err)
        {
          console.log(` Error occurred while inserting values inside the table \n${err}`);
        }
     });
    // use this without director data.
    if (!directorMap.hasOwnProperty(director)) {
    pool.query("INSERT INTO directors (director_name) \ VALUES($1)", 
      [director], function(err, res){
        if(err)
        {
          console.log(` Error occurred while inserting values inside the table \n${err}`);
        }
    });
  }
  const query = {
    text: 'SELECT id FROM directors WHERE director_name = $1',
    values: [director],
  }

  pool
  .query(query)
  .then(res => {
    var director_id = res.rows[0].id;
    pool.query("INSERT INTO newmoviestable(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,Director_id,Actor,Year) \ VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)", 
      [rank,title,description,runtime,genre,rating,metascore,votes,gross_earning_in_mil,director_id,actor,year], function(err){
        if(err)
        {
          console.log(` Error occurred while inserting values inside the table \n${err}`);
        }
    });
  })
  .catch(function(err) {
    console.error(err.stack)
  });


  // resume/ continue reading the csv after inserting the data in that line in the table  
  moviesCsvStream.resume();
  }).on("end", function(){
    console.log("Tables created using csv");
  });