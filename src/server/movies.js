const { Pool } = require("pg");
const credentials = require('./credentials');

const pool = new Pool({
    user: credentials.username,
    password: credentials.password,
    database: credentials.database,
    host: credentials.server
});

pool.connect(function(err){
    if(err)
    {
        console.log(`Error occurred while connecting \n${err}`);
    }
  });

  module.exports = {
    getMovies: function() {
        return new Promise(function(resolve, reject) {
          const moviesQuery = 'SELECT * FROM newmoviestable';
          pool.query(moviesQuery).then(function(res) {
            resolve(res.rows);
          }).catch(function(err) {
            reject(err);
          });
        });
      },

      getMovieWithID: function(app) {
        return new Promise(function(resolve, reject) {
            app.get('/api/movies/:movieId', function(req, res) {
              const movieIdQuery = `SELECT * FROM newmoviestable WHERE rank = ${req.params.movieId}`;
              pool.query(movieIdQuery).then(function(res1) {
                  res.send(res1.rows);
                  resolve(res1.rows);
              }).catch(function(err) {
                  reject(err);
              });
          });
      });  
    },

    addMovie: function(app) {
        return new Promise(function(resolve, reject) {
          app.post('/api/movies', function(req, res) {
              if (req.body.rank === undefined)
                {
                  res.status(404).send("rank column cannot be empty.enter any unique rank");
                  resolve("rank column cannot be empty.enter any unique rank");
                }
              else
                {
                  let data = {};
                  if (req.body.hasOwnProperty('rank'))
                    {
                      data.rank = req.body.rank;
                    }
                    if (req.body.hasOwnProperty('title'))
                    {
                      data.title = req.body.title;
                    }
                  else 
                    {
                      data.title = null;
                    }
                    if (req.body.hasOwnProperty('description'))
                    {
                      data.description = req.body.description;
                    }
                  else 
                    {
                      data.description = null;
                    }
                    if (req.body.hasOwnProperty('runtime'))
                    {
                      data.runtime = req.body.runtime;
                    }
                  else 
                    {
                      data.runtime = null;
                    }
                    if (req.body.hasOwnProperty('genre'))
                    {
                      data.genre = req.body.genre;
                    }
                  else 
                    {
                      data.genre = null;
                    }
                    if (req.body.hasOwnProperty('rating'))
                    {
                      data.rating = req.body.rating;
                    }
                  else 
                    {
                      data.rating = null;
                    }
                    if (req.body.hasOwnProperty('metascore'))
                    {
                      data.metascore = req.body.metascore;
                    }
                  else 
                    {
                      data.metascore = null;
                    }
                    if (req.body.hasOwnProperty('votes'))
                    {
                      data.votes = req.body.votes;
                    }
                  else 
                    {
                      data.votes = null;
                    }
                    if (req.body.hasOwnProperty('gross_earning_in_mil'))
                    {
                      data.gross_earning_in_mil = req.body.gross_earning_in_mil;
                    }
                  else 
                    {
                      data.gross_earning_in_mil = null;
                    }
                    if (req.body.hasOwnProperty('actor'))
                    {
                      data.actor = req.body.actor;
                    }
                  else 
                    {
                      data.actor = null;
                    }
                    if (req.body.hasOwnProperty('year'))
                    {
                      data.year = req.body.year;
                    }
                  else 
                    {
                      data.year = null;
                    }
                    const addMovieQuery = 'INSERT INTO newmoviestable(rank, title, description, runtime, genre, rating, metascore, votes, gross_earning_in_mil, actor, year) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)';
                    const value = [data.rank, data.title, data.description, data.runtime, data.genre, data.rating, data.metascore, data.votes,
                    data.gross_earning_in_mil, data.actor, data.year];
                    pool.query(addMovieQuery, value).then(function(res1) {
                      res.send(res1)
                      resolve(res1);
                    }).catch(function(err) {
                      reject(err);
                    }); 
                }
          });
        });
      },
  
      updateMovieWithId: function(app) {
        return new Promise(function(resolve, reject) {
          app.put('/api/movies/:movieId', function(req, res) {
            let data = req.body;
            const movieIdQuery = `UPDATE newmoviestable SET title = $1,
                                                            description = $2, 
                                                            runtime = $3, 
                                                            genre = $4,
                                                            rating = $5,
                                                            metascore = $6, 
                                                            votes = $7, 
                                                            gross_earning_in_mil = $8, 
                                                            actor = $9, 
                                                            year = $10
                                                            WHERE rank = ${req.params.movieId}`;
            const value = [data.title, data.description, data.runtime, data.genre, data.rating, data.metascore, 
                           data.votes,data.gross_earning_in_mil, data.actor, data.year];
            pool.query(movieIdQuery, value).then(function(res1) {
                res.send(res1);
                resolve(res1);
            }).catch(function(err) {
                reject(err);
            });
          });
        });  
      },
  
      deleteMovieWithId: function(app) {
        return new Promise(function(resolve, reject) {
          app.delete('/api/movies/:movieId', function(req, res) {
            const movieIdQuery = `DELETE FROM newmoviestable where rank = ${req.params.movieId}`;
            pool.query(movieIdQuery).then(function(res1) {
                res.send(res1);
                resolve(res1);
            }).catch(function(err) {
                reject(err);
            });
        });
    });  
  }
}