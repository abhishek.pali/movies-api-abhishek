const options = {};

if (process.env.NODE_ENV !== "production") {
    options.overrideProcessEnv = true;

    require('dotenv-extended').load(options);
}

var fs = require("fs");
const config = require("./config");

const Sequelize = require('sequelize');
const express = require('express');

const sequelize = new Sequelize(config.mysql.database, config.mysql.username, config.mysql.password, {
    dialect: 'mysql',
    host: config.mysql.host,
    dialectOptions: {
        connectTimeout: config.mysql.timeout,
    },
    pool: {
        max: config.mysql.pool.max,
        min: config.mysql.pool.min,
        acquire: config.mysql.pool.acquire,
        idle: config.mysql.pool.idle,
    },
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');

        const directorsModel = require("./models/directors")(sequelize);
        const moviesModel = require("./models/movies")(sequelize);

        // directorsModel.hasMany(moviesModel)
        // moviesModel.belongsTo(directorsModel)
        // sequelize.sync().then(() => {
        //     console.log('Tables creation successful')
        //     fs.readFile('../data/movies.json', 'utf-8', (err, data) => {
        //         if (err) {
        //             console.log(`ERROR => Cannot read data from the file :\n${err.message}`)
        //         } else {
        //             let moviesData = JSON.parse(data)
        //             let directorData = moviesData.map(movie => {
        //                 return movie.Director
        //             })
        //             let directorSet = new Set(directorData)         //to only take distinct director names
        //             let directorArray = Array.from(directorSet)         //create an array from a set
        //             // map the array of director names to an array of objects containing director names
        //             directorArray = directorArray.map(director => {
        //                 let directorName = {}
        //                 directorName['Director'] = director
        //                 return directorName
        //             })
        //             directorsModel.bulkCreate(directorArray)
        //                 .then(() => directorsModel.findAll({ raw: true }))
        //                 .then(data => {
        //                     console.log(data)
        //                 }).catch(err => {
        //                     if (err.message === 'Validation error') {
        //                         console.log(`ERROR => Data already present (duplicate data)`)
        //                     } else {
        //                         console.log(`ERROR => Data not Inserted : ${err.message}`)
        //                     }
        //                 })
        //             let dataForMoviesTable = moviesData
        //             dataForMoviesTable.map(movieInfo => {
        //                 directorsModel.findOne({
        //                     raw: true,
        //                     attributes: ['id'],
        //                     where: {
        //                         Director: `${movieInfo.Director}`
        //                     }
        //                 }).then(directorId => {
        //                     movieInfo['directorId'] = directorId.id
        //                     delete movieInfo.Director
        //                     if (movieInfo.Metascore === 'NA') {
        //                         delete movieInfo.Metascore
        //                     }
        //                     if (movieInfo.Gross_Earning_in_Mil === 'NA') {
        //                         delete movieInfo.Gross_Earning_in_Mil
        //                     }
        //                     moviesModel.create(movieInfo)
        //                         .then(() => console.log('Data Inserted'))
        //                         .catch(err => {
        //                             if (err.message === 'Validation error') {
        //                                 console.log(`ERROR => Data already present (duplicate data)`)
        //                             } else {
        //                                 console.log(`ERROR => Data not Inserted : ${err.message}`)
        //                             }
        //                         })
        //                 })
        //             })
        //             setTimeout(() => {
        //                 apiRoutes(moviesModel, directorsModel, config.port)
        //             }, 2000)
        //         }
        //     })
        // })


        // sequelize.sync()
        //     .then(() => {
        //         const app = express();

        //         app.listen(config.port, () => {
        //             console.log(`App listening on port ${config.port}!`)
        //         });
        //     })
        //     .catch((err) => {
        //         throw err;
        //     });
    })
    .catch(err => {
        throw err;
    });
